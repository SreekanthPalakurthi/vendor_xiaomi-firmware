# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi-firmware/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi-firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi-firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi-firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi-firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi-firmware/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi-firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi-firmware/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/xiaomi-firmware/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi-firmware/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi-firmware/multi_image.mbn:install/firmware-update/multi_image.mbn \
    vendor/xiaomi-firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi-firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi-firmware/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi-firmware/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi-firmware/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
    vendor/xiaomi-firmware/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/xiaomi-firmware/xbl.elf:install/firmware-update/xbl.elf